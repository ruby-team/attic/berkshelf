require 'gem2deb/rake/spectask'

Gem2Deb::Rake::RSpecTask.new(:spec) do |spec|
  spec.pattern = './spec/**/*_spec.rb'
end

require 'cucumber'
require 'cucumber/rake/task'
Cucumber::Rake::Task.new(:features) do |t|
  t.bundler = false
  t.cucumber_opts = %W{
    features
    -x
    --format progress
    --tags ~@ignore
  }
end

task :default => [:spec, :features]
